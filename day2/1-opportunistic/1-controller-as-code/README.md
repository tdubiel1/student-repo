Run the `ansible-navigator` command with the `run` argument and -m stdout as well as `--extra-vars`

- The setup.yml playbook will configure the following in the Automation Controler.
  - A new Project named 'Student Project'
  - A new Execution Environment named 'Validate Network'
  - A new Credential Type named 'Gitlab Credential'
  - Make changes to the inventory 

```bash
[student@ansible-1 1-controller-as-code]$ ansible-navigator run setup.yml -m stdout --extra-vars "username=redacted token=redacted password=redacted"