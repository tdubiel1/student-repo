Run the `ansible-navigator` command with the `run` argument and -m stdout as well as `--extra-vars`

```bash
$ ansible-navigator run setup.yml -m stdout --extra-vars "username=redacted token=redacted password=redacted"